describe('Login', () => {
  it('passes', () => {
     cy.visit('https://app.privy.id')
    
    cy.visit('https://app.privy.id')
    cy.title().should('equal', 'PrivyID Oauth')
    cy.get('#__BVID__4').type('EIH2733')

    cy.contains('button', 'CONTINUE').as('loginBtn') // button camel case => btnLogin
    cy.get('@loginBtn').click()

    cy.get('#__BVID__6').type('Egiizul123')
    cy.get('@loginBtn').click()

    cy.contains('button', 'Upload Document').as('btnUploadDocs')
    cy.get('@btnUploadDocs').click()
    cy.wait(3000)

    cy.contains('Self Sign').as('btnSelfSign')
    cy.get('@btnSelfSign').click()
    cy.wait(3000)

    cy.go('back')

    cy.contains('button', 'Upload Document').as('btnUploadDocs')
    cy.get('@btnUploadDocs').click()
    cy.wait(3000)


    cy.contains('Sign & Request').as('btnSignReq')
    cy.get('@btnSignReq').click()
    cy.wait(3000)

    cy.go('back')

    cy.contains('Change My Signature Image').as('btnChangeImage')
    cy.get('@btnChangeImage').click()
    cy.wait(3000)

    






  })
})